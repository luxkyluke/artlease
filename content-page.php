<div class="page">
	<?php $concept = get_field('concept'); if (!empty($concept)): ?>
		<div class="section-container-concept section-container">
			<div 
				class="image-header background-img" 
				data-background-img="<?php echo get_field('fond_1')['url']; ?>" 
				data-img-tablet="<?php echo get_field('fond_1')['sizes']['large']; ?>"
				>
				<div class="top-overhang col-md-8 col-sm-10 col-xs-12">
					<div class="top-overhang-white"></div>
					<div class="header-statement">
						<div class="statement">
							<div class="contents-slogan" >
								<div class="contents-slogan-wrapper">
									<?php echo getFatTitle(get_the_title()) ?>
									<?php echo get_field('slogan'); ?>
								</div>							
							</div>
						</div>
					</div>
					<div class="concept-contents contents">
						<div class="conpt-contents-wrapper contents-wrapper ">
							<?php echo $concept; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php $conditions = get_field('conditions');if (!empty($conditions)): ?>
		<div class="section-container-conditions section-container" >
			<div 
				class="image-header" 
				data-background-img="<?php echo get_field('fond_2')['url']; ?>"
				data-img-tablet="<?php echo get_field('fond_2')['sizes']['large']; ?>"
				>
				<div class="top-overhang-white-wrapper col-md-4 col-sm-2 col-xs-0">
					<div class="top-overhang-white"></div>
					<div class="top-overhang-white end"></div>
				</div>
				<div class="top-overhang col-md-8 col-sm-10 col-xs-12">
					<div class="contents-slogan"></div>
					<div class="conditions-contents contents">
						<div class="conpt-contents-wrapper contents-wrapper">
							<?php echo $conditions; ?>
						</div>
					</div>
					<div class="contents-slogan end"></div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php $conseil = get_field('conseil'); if (!empty($conseil)): ?>
		<div class="section-container-conseil section-container" >
			<div 
				class="image-header" 
				data-background-img="<?php echo get_field('fond_3')['url']; ?>"
				data-img-tablet="<?php echo get_field('fond_3')['sizes']['large']; ?>"
				>
				<div class="top-overhang col-md-8 col-sm-10 col-xs-12">
					<div class="contents-slogan"></div>
					<div class="conseil-contents contents">
						<div class="conpt-contents-wrapper contents-wrapper">
							<?php echo $conseil; ?>
						</div>
					</div>
					<div class="contents-slogan end"></div>
				</div>
				<div class="top-overhang-white-wrapper col-md-4 col-sm-2 col-xs-0">
					<div class="top-overhang-white"></div>
					<div class="top-overhang-white end"></div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="section-container-realisations" >
		<div class="top-overhang col-md-8 col-sm-10 col-xs-12">
			<div class="contents">
				<div class="contents-wrapper">
					<h2 class="title"><?php echo get_gallery_title();?></h2>
					
				</div>
				
			</div>
		</div>
	
		<?php include(__DIR__ .'/gallery.php') ;?>
	</div>
</div>
