<?php 
	$img = get_field('photo')['url'];
?>

<div class="page artiste" data-hide-header-scroll="true">
	<div class="photo-container">
		<div class="photo" data-background-img="<?php echo $img; ?>"></div>
	</div>
	<div class="text-container">
		<h1 class="title"><?php the_title(); ?></h1>
		<div class="bio-container">
			<div class="bio">
				<?php
				$bio = get_field("bio");
				$splitedBio = explode("<p>", $bio);
				foreach ($splitedBio as $i => $p):
					$para = str_replace("<p>", "", $p);
					$para = str_replace("</p>", "", $para);?>
		
					<div class="paragraph">
						<?php 
						if($i == 1):?>
							<div> <?php echo $para;?></div>
							<div class="photo-container tablet">
								<div class="photo" data-background-img="<?php echo $img; ?>"></div>
							</div>
						<?php else:?>
							<?php echo $para;?>
						<?php endif; ?>
					</div>
					
				<?php endforeach;?>
			</div>
		</div>
	</div>
</div>
<h2 class="title oeuvres">Ses Oeuvres </h2>
