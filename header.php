<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-110577987-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-110577987-1');
	</script> -->
	<!-- UIkit JS -->

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<?php wp_head(); ?>
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico" type="image/x-icon">
</head>

<body <?php body_class(); ?>>
	<?php //include(__DIR__ .'/gallery-full.php') ;?>
	<?php include(__DIR__ .'/contact-full.php') ;?>
	<?php include(__DIR__ .'/loader.php') ;?>
	<div id="scroll-wrapper">
		<div class="frame"></div>
		<!--[if !IE]><!-->
		<!--<![endif]-->
		<?php include(__DIR__ .'/burger-menu.php');?>
		<header class="mobile-header">
			<div id="burger-menu-background"></div>
			<a href="#" id="burger-menu-btn-link">
				<div class="burger-btn-wrapper">
				<div id="burger-menu-btn">
	                <div >
	                    <span class="burger-menu-span"></span>
	                </div>
	            </div>
			</div></a>
		</header>
		<header id="header" class="header">
			<div class="inner-content">
				<a href="<?php echo home_url(); ?>" class="logo">
					<img src="<?php echo get_template_directory_uri(); ?>/img/logo-black.svg" alt="Logo" >
				</a>

				<nav class="navigation">
					<?php 
						$menu = wp_nav_menu(array(
							'theme_location' => 'superior'
						));
					?>
					<?php echo getMainMenu(); ?>
				</nav>
			</div>
		</header>

		<div id="page">	

