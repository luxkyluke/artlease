<?php get_header(); ?>
<!-- container -->
<div class="container">
	<div id="primary" class="not-found">
		<section class="error-404 not-found">
			<h1 class="page-title"><?php _e( "Oups ! nous n'avons pas pu trouver cette page " ); ?></h1>
			<div class="page-content">
				<a href="<?php echo home_url(); ?>" class="button" style="width: 300px; margin: 50px auto;">retour à l'acceuil</a>

			</div>
		</section>
	</div>
</div>
<!-- /container -->
<?php get_footer(); ?>
