<div class="page about" data-hide-header-scroll="true">
	<div class="cover row">
		<div id="cover-img" class="cover-img col-xs-12 col-sm-10 col-sm-offset-2 col-md-8 col-md-offset-4">
			<div 
				class="background-layer background-img" 
				data-background-img="<?php echo get_field('cover-img')['url'];?>"
				data-img-tablet="<?php echo get_field('cover-img')['sizes']['large']; ?>"
				></div>
			<div class="overlay"></div>
		</div>
		<div id="slogan" class="slogan col-xs-11 col-xs-offset-1 col-sm-8 col-md-6 col-md-offset-2 col-xl-4 col-xl-offset-3" >
			<?php echo get_field('slogan') ?>
		</div>
	</div>
	<div class="container">
		<div class="row center-xs">
			<div class="content  col-xs-12 col-sm-8 col-md-6 col-xl-4">
				<?php echo get_field('content') ?>
			</div>
		</div>
	</div>
	<div class="banniere-wrapper">
		<div 
			class="banniere" 
			data-background-img="<?php echo get_field('banner-img')['url'] ?>"
			data-img-tablet="<?php echo get_field('banner-img')['sizes']['large']; ?>"
		></div>
	</div>
	<div class="row team-section center-xs">
		<div class="col-xs-11 col-sm-11 col-md-10 col-lg-8">
			<?php if( have_rows('team') ): ?>
				<div class="team">
					<h2 class="title">Notre équipe</h2>
					<div class="member-container">						
						<?php $i=0; 
							while( have_rows('team') ): the_row(); 
								$photo = get_sub_field('photo');
								$name = get_sub_field('name');
								$function = get_sub_field('function');
								$i++;
							?>
							<div class="team-member">
								<div  class="background-layer" data-background-img="<?php echo $photo['sizes']['large'] ;?>"></div>
								<div class="overlay"></div>
								<div class="infos">
									<h3 class="name"><?php echo $name ?></h3>
									<p class="function"><?php echo $function ?></p>
								</div>
							</div>
							<!-- <?php if ($i%3 == 0): ?>
								</div><div class="member-container">
							<?php endif; ?> -->
						<?php endwhile; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="button-wrapper">
		<div class="button">
			<a href="#contact">
				Nous contacter
			</a>
		</div>
	</div>
</div>
