<?php
function wordpressify_resources() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );
	wp_enqueue_script( 'header_js', get_template_directory_uri() . '/js/header-bundle.js', null, 1.0, false );
	wp_enqueue_script( 'footer_js', get_template_directory_uri() . '/js/footer-bundle.js', null, 1.0, true );
}

add_action( 'wp_enqueue_scripts', 'wordpressify_resources' );

// Customize excerpt word count length
function custom_excerpt_length() {
	return 22;
}

add_filter( 'excerpt_length', 'custom_excerpt_length' );

// Theme setup
function wordpressify_setup() {
	// Handle Titles
	add_theme_support( 'title-tag' );

	// Add featured image support
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'small-thumbnail', 720, 720, true );
	add_image_size( 'square-thumbnail', 80, 80, true );
	add_image_size( 'banner-image', 1024, 1024, true );

	add_image_size( 'extract-thumb', 250, 250, true ); 
}

add_action( 'after_setup_theme', 'wordpressify_setup' );

show_admin_bar( false );

// Checks if there are any posts in the results
function is_search_has_results() {
	return 0 != $GLOBALS['wp_query']->found_posts;
}

// Add Widget Areas
function wordpressify_widgets() {
	register_sidebar(
		array(
			'name'          => 'Sidebar',
			'id'            => 'sidebar1',
			'before_widget' => '<div class="widget-item">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}

add_action( 'widgets_init', 'wordpressify_widgets' );


register_nav_menus(array(
	'main' 		=> __('Main Menu'),
	'superior' 	=> __('Superior Menu')
));

function getFatTitle($title){
	$splitTitle = explode(' ', $title);
	       						
	$fatTitle = $splitTitle[0];

	array_shift($splitTitle);
	$title = implode(' ', $splitTitle);
	return '<strong>'.$fatTitle .'</strong> '. $title;
}

function getMainMenu(){
	$menu_name = 'main';
	 
	if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
	    $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
	 
	    $menu_items = wp_get_nav_menu_items($menu->term_id);
	 
	    $menu_list = '<div class="menu-' . $menu_name . '-container">';
	    $menu_list .= '<ul id="menu-' . $menu_name . '" class="menu">';
	 

	   	$cpi=get_the_ID();

	   	//determine la page courante
	   	foreach( $menu_items as $current ) {
	        if($cpi == $current->object_id ){if ( !$current->menu_item_parent ) {$cpi=$current->ID;}else{$cpi=$current->menu_item_parent;}$cai=$current->ID;break;}
	    }
	    foreach( $menu_items as $menu_item ) {
	        $link = $menu_item->url;
	        $title = $menu_item->title;

	        $menu_item->ID==$cpi ? $ac=' current-menu-item' : $ac='';
	        if ( !$menu_item->menu_item_parent ) {
	            $menu_list .= '<li class="'.$ac.'">' ."\n";$menu_list .= '<a href="'.$link.'"><span>'. getFatTitle($title) .'</a>' ."\n";
			}
             
		}

	    $menu_list .= '</ul>';
	    $menu_list .= '</div>';
	} else {
	    $menu_list = '<ul><li>Menu "' . $menu_name . '" not defined.</li></ul>';
	}
	return $menu_list;
};

function getBurgerMenu(){
	$menu_name = 'superior';
	 
	if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
	    $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
	 
	    $menu_items = wp_get_nav_menu_items($menu->term_id);
	 
	    $menu_list = '<div class="menu-' . $menu_name . '-container">';
	    $menu_list .= '<ul id="menu-' . $menu_name . '" class="menu">';
	 
	    $i = 0;
	    foreach( $menu_items as $menu_item ) {
	        $link = $menu_item->url;
	        $title = $menu_item->title;
	        if ( !$menu_item->menu_item_parent ) {
	            if($i == 1){
	            	$menu_list .= '<li>' ."\n";
	            	$menu_list .= '<a href="#">Services</a></li>' ."\n";
	            	$menu_list .= getMainMenu();
	            }
	            $menu_list .= '<li>' ."\n";
	            $menu_list .= '<a href="'.$link.'">'. $title .'</a></li>' ."\n";
			}
            $i++;
		}

	    $menu_list .= '</ul>';
	    $menu_list .= '</div>';
	} else {
	    $menu_list = '<ul><li>Menu "' . $menu_name . '" not defined.</li></ul>';
	}
	return $menu_list;
}



/*=================
  CUSTOM POST TYPE
===================*/

function artiste_custom_post_type(){
	$labels = array(
		'name' => 'Artistes',
		'singular_name' => 'Artiste',
		'add_new' => 'Ajouter un Artiste',
		'all_items' => 'Tous les artistes',
		'all_new_items' => 'Ajouter un artiste',
		'edit_item' => 'Modifier l\'artiste',
		'new_item' => 'Nouvel artiste',
		'view_item' => 'Afficher l\'artiste',
		'search_item' => 'Rechercher un artiste',
		'not_found' => 'Pas d\'artiste trouvé',
		'not_found_in_trash' => 'Pas d\'artiste trouvé dans la corbeille',
		'parent_item_colon' => 'Item Parent',

	);
	$rewrite = array(
		'slug'                  => 'artiste',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);

	$args = array(
		'label'                 => __( 'Artiste', 'text_domain' ),
		'description'           => __( 'le type artiste', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'custom-fields', 'thumbnail'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);

	register_post_type( 'artiste', $args );
}
add_action( 'init', 'artiste_custom_post_type', 0 );

// Register Custom Post Type
function oeuvre_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Oeuvres', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Oeuvre', 'Post Type Singular Name', 'text_domain' ),
		'not_found' => 'Aucune oeuvres trouvée',
	);
	$rewrite = array(
		'slug'                  => 'oeuvre',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Oeuvre', 'text_domain' ),
		'description'           => __( 'le type oeuvre', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'oeuvre', $args );

}
add_action( 'init', 'oeuvre_custom_post_type', 0 );

// POSTS 2 POSTS
function oeuvre_artiste_connection_types() {
	p2p_register_connection_type( array(
		'name' => 'oeuvre_to_artiste',
		'from' => 'oeuvre',
		'to' => 'artiste',
		'reciprocal' => true,
		'admin_column' => 'any'
	) );
}
add_action( 'p2p_init', 'oeuvre_artiste_connection_types' );

//retourne le lien google maps de la position
function getGoogleMapsLink($location){
	$str =  'https://www.google.fr/maps/place/'.$location;
	return $str;
}

//retourne les infos nécessaire à la gallery full
function get_gallery_item_content_callback() {

    // retrieve post_id, and sanitize it to enhance security
    $postId = intval($_POST['post-id'] );
    $imgId = intval($_POST['img-id'] );

    // Check if the input was a valid integer
    if ( $postId == 0 ) {
        $response['error'] = 'true';
        $response['result'] = 'Invalid Input';

    } else {
        // get the post
        $thispost = get_post( $postId );

        // check if post exists
        if ( !is_object( $thispost )) {
            $response['error'] = 'true';
            $response['result'] =  'There is no post with the ID ' . $postId;

        } else {
        	$gallery = get_field('gallery', $postId);

        	if ( empty($gallery)){
        		
        		$response['error'] = 'true';
            	$response['result'] =  'There is no gallery in this post ID ' . $postId;

        	}else{
        		$img = $gallery[$imgId];

        		if (empty( $img )){
	        		$response['error'] = 'true';
	            	$response['result'] =  'There is no image in the id ' . $postId.' in the gallery';
	        	
	        	}else{
		            $response['error'] = 'false';

		            $photo = $img;
		            $photo["artiste"] = "Stéphane Lombard";
		            $photo["title"] = "Négligence";
		            $photo["date"] = date("Y", strtotime($img["date"]));
		            $photo["artiste-url"] = "https://fr.wikipedia.org/wiki/Michel_Sardou";

		            $response['result'] = $photo;
		        }
        	}
        }

    }

    wp_send_json( $response );

}

add_action( 'wp_ajax_get_gallery_item_content', 'get_gallery_item_content_callback' );
// If you want not logged in users to be allowed to use this function as well, register it again with this function:
add_action( 'wp_ajax_nopriv_get_gallery_item_content', 'get_gallery_item_content_callback' );


function actuality_loadmore_ajax_handler(){
 
	// prepare our arguments for the query
	$args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged'] = $_POST['page']+1 ; // we need next page to be loaded
	$args['post_status'] = 'publish';
	$args['category_name'] = 'Actualités';
 
	// it is always better to use WP_Query but not here
	query_posts( $args );
 
	if( have_posts() ) :
 
		// run the loop
		while( have_posts() ): the_post();
			$cpt++;
			$odd = ($cpt%2) ? '' : 'odd'; 
			echo '<div class="post-wrapper row '.$odd.'">';
 
			// look into your theme code how the posts are inserted, but you can use your own HTML of course
			// do you remember? - my example is adapted for Twenty Seventeen theme
			get_template_part( 'content-extract', get_post_format() );
			// for the test purposes comment the line above and uncomment the below one
			// the_title();
 			echo '</div>';
 
		endwhile;
 
	endif;
	die; // here we exit the script and even no wp_reset_query() required!
}
 
 
 
add_action('wp_ajax_loadmore', 'actuality_loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'actuality_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}







//////////////////////// UTILITY ///////////////////////////////




function getYoutubeVideoId($url){
	parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
	return $my_array_of_vars['v'];  
}

function getVimeoVideoId($url){
	if (preg_match("/(?:https?:\/\/)?(?:www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/", $url, $id)) {
	    $videoId = $id[3];
	}
	return $videoId;
}

function getYoutubeEmbedUrl($id){
	return "https://www.youtube.com/embed/".$id;
}

function getVimeoEmbedUrl($id){
	return "https://player.vimeo.com/video/".$id;
}

function getInfosYoutubeVideo($videoUrl){
	$id = getYoutubeVideoId($videoUrl);
	$infos = Array();
	$infos['large-img'] = "https://img.youtube.com/vi/".$id."/hqdefault.jpg";
	$infos['small-img'] = "https://img.youtube.com/vi/".$id."/default.jpg";
	$infos['embed'] = getYoutubeEmbedUrl($id);

	return $infos;
}

function getInfosVimeoVideo($videoUrl){
	$id = getVimeoVideoId($videoUrl);
	$video = file_get_contents("http://vimeo.com/api/v2/video/".$id.".php");
	$vimeo = unserialize($video);

	$infos = Array();
	$infos['large-img'] = $vimeo[0]['thumbnail_large'];
	$infos['small-img'] = $vimeo[0]['thumbnail_small'];
	$infos['embed'] = getVimeoEmbedUrl($id);

	return $infos;
}

function videoIsYoutube($url){
	return strpos($url, "youtube");
}

function videoIsVimeo($url){
	return strpos($url, "vimeo");
}


function getOeuvreFromAFCPhoto($img, $oeuvreId){
	$item = formatACFImageToGalleryItem($img);
	
	$itemInfos = getOeuvreInfoItem($oeuvreId);

	$item = array_merge($item, $itemInfos);

	$item["isVideo"] = false;
	$item["isOeuvre"]	= true;
	return $item;
}

function getOeuvrePhoto($oeuvreId){
	$img = get_field('gallery', $oeuvreId)[0];

	$item = getOeuvreFromAFCPhoto($img, $oeuvreId);

	//concatener les info avec l'item
	return $item;
}

function getOeuvrePhotos($oeuvreId){
	$photos = get_field('gallery', $oeuvreId);

	if(empty($photos)){
		return null;
	}

	$oeuvres = Array();
	foreach ($photos as $i => $p) {
		$oeuvre = getOeuvreFromAFCPhoto($p, $oeuvreId);
		array_push($oeuvres, $oeuvre);
	}

	return $oeuvres;
}

function getOeuvreArtistes($oeuvreId){
	$oeuvrePost = get_post($oeuvreId);
	if( !empty($oeuvresPost) )
		return NULL;

	$artistes = Array();

	$artistesPosts = p2p_type( 'oeuvre_to_artiste' )->set_direction( 'from' )->get_connected( $oeuvreId );
	if ( $artistesPosts && $artistesPosts->have_posts() ) {
		while ( $artistesPosts->have_posts() ){
			$artistesPosts->the_post();
			
			$artiste = Array();
			
			$artiste['name'] = get_the_title(); 
			$artiste['url'] = get_the_permalink(); 

			array_push($artistes, $artiste);
		}
	}

	wp_reset_postdata();

	return $artistes;

}

function getOeuvreInfoItem($oeuvreId){
	// if(!is_int($oeuvreId))
	// 	return NULL;

	$item = Array();
	$oeuvreTitle = get_the_title($oeuvreId);
	$oeuvreDetail = get_field("detail", $oeuvreId);
	$oeuvreDate = get_field("date", $oeuvreId);
	$artistes = getOeuvreArtistes($oeuvreId);

	//si on veut toute les noms d'artiste séparer par un <br>
	// $combined = array_merge(...$artistes);
	// var_dump($combined);
	// $artistesNames = implode("<br>", $combined['name']);
	
	$artistsLabel = "";
	foreach ($artistes as $i => $a) {
		if($i > 0){
			$artistsLabel.=" <br>& ";
		}
		$artistsLabel .= $a['name'];
	}

	$name = $artistsLabel;

	// if (!isset($name)){
	// 	$name = "";
	// }

	$item["caption"] = $oeuvreTitle;
	$item["description"] = $name; 
	$item["artistes"] = $artistes;
	$item["detail"] = $oeuvreDetail;
	$item["date"] = $oeuvreDate;

	return $item;
}	

function getVideoInfos($videoUrl){
	//si c'est une video youtube
	if(videoIsYoutube($videoUrl)){
		return getInfosYoutubeVideo($videoUrl);

	}elseif(videoIsVimeo($videoUrl)){		//si c'est une video vimeo
		return getInfosVimeoVideo($videoUrl);
	}
}

function getOeuvreItemGallery($oeuvreId){
	$videoUrl = get_field('video_link', $oeuvreId);
	$videoInfos = NULL;
	if(!empty($videoUrl)){
		
		$videoInfos = getVideoInfos($videoUrl);
		
		if(isset($videoInfos)){

			$item = Array();
			$oeuvreInfo = getOeuvreInfoItem($oeuvreId);

			$item = array_merge($item, $videoInfos);
			$item = array_merge($item, $oeuvreInfo);

			$item["isOeuvre"]	= true;
			$item["isVideo"] 	= true;
	
			// $item["large-img"] 		= $videoInfos['large-img'];
			// $item["small-img"] 		= $videoInfos['small-img'];
			// $item["caption"] 		= $oeuvreInfo['caption'];
			// $item["description"] 	= $oeuvreInfo['description'];

			return $item;
		}
	}
	return getOeuvrePhoto($oeuvreId);
}

function formatACFImageToGalleryItem($image){
	$item = Array();
	
	$item["large-img"] 		= $image['sizes']['large'];
	$item["small-img"] 		= $image['sizes']['thumbnail'];
	$item["banner-img"] 	= $image['sizes']['banner-image'];
	$item["caption"] 		= $image['caption'];
	$item["description"] 	= $image['description'];
	// $item["id"] 			= $image['id'];
	$item["file"] 			= $image['filename'];
	$item["alt"] 			= $image['alt'];
	$item["isOeuvre"]		= false;

	return $item;
}


function formatACFVideoToGalleryItem($video){
	if(empty($video['url']))
		return null;

	$videoInfos = getVideoInfos($video['url']);

	$item = Array();

	$item["large-img"] 		= $videoInfos['large-img'];
	$item["small-img"] 		= $videoInfos['small-img'];
	$item["embed"] 			= $videoInfos['embed'];
	$item["caption"] 		= $video['caption'];
	$item["description"] 	= $video['description'];
	$item["isOeuvre"]		= false;
	$item["isVideo"] 		= true;

	return $item;
}


function getGalleryItemsForPage($pageName){
	$allOeuvres = get_posts(array(
		'post_type' 	=> 'oeuvre',
		'numberposts'	=> '-1'
	));
	$items = Array();

	foreach ($allOeuvres as $i => $o) {
		$page = get_field('page', $o->ID);

		if (strpos($page, $pageName)){
			$item = getOeuvreItemGallery($o->ID);
			if(!isset($item))
				continue;
			array_push($items, $item);
		}
	}
	return $items;
}

function getGalleryItemsForArtiste($artisteName){
	$oeuvres = get_posts( array(
	  	'connected_type' => 'oeuvre_to_artiste',
	  	'connected_items' => get_queried_object(),
	  	'nopaging' => true,
		'numberposts'	=> '-1',
	  	'suppress_filters' => false
	) );
	
	$items = Array();

	foreach ($oeuvres as $i => $o) {
		$item = getOeuvreItemGallery($o->ID);

		if(!isset($item))
			continue;

		array_push($items, $item);

		$galleryImages = getOeuvrePhotos($o->ID);

		if(!empty($galleryImages)){
			$items = array_merge($galleryImages, $items);
		}
	}
	return $items;
}

function getItemFromACFImage($postId){
	$galleryItems = get_field('gallery', $postId);
	if($galleryItems){
		foreach ($galleryItems as $i => $img) {
			$galleryItems[$i] = formatACFImageToGalleryItem($img);
		}
	}else{
		$galleryItems = Array();
	}

	$rows = get_field('videos');
	if($rows){
		foreach($rows as $row){
			$video = formatACFVideoToGalleryItem($row);
			if(!empty($video)){
				array_push($galleryItems, $video);
			}
		}

	}

	return $galleryItems;
}

function get_gallery_items(){

	if(is_page()){
		$post = get_post();
		$pageName = $post->post_name;		
		$items = getGalleryItemsForPage($pageName);
	}

	if(get_post_type() == 'artiste'){
		$artisteName =  get_post()->post_name;
		$items = getGalleryItemsForArtiste($artisteName);
	}else{
		$galleryImages = getItemFromACFImage(get_the_id());
		if(!empty($items)){
			$galleryImages = array_merge($galleryImages, $items);
		}
		
		$items = $galleryImages;
	}	

	foreach ($items as $i => $item) {
		$items[$i]['id'] = $i;
	}

	shuffle($items);
	
	return $items;	
}

function getMainPageInfo($page){
	$infos = Array();

	$infos['url'] = get_permalink($page->ID);
	$infos['title'] = $page->post_title;
	$infos['slogan'] = get_field('slogan', $page->ID);
	$infos['bg'] = get_field('fond_1', $page->ID)['url'];
	$infos['bg-mobile'] = get_field('fond_1', $page->ID)['sizes']['medium'];

	return $infos;
}

function getNextInfo($singleID){
	$infos = Array();

	$infos['url'] = get_permalink($singleID);
	$infos['title'] = get_the_title($singleID);
	$infos['bg'] = get_field('cover', $singleID)['url'];
	$infos['bg-mobile'] = get_field('cover', $singleID)['sizes']['medium'];

	return $infos;
}



function getNextPageInfo($label, $args){

	$neighbours = getNeighbourIDsFromPosts($args);

	//if has next
	if($neighbours['nextid']>0){
		$infos = getNextInfo($neighbours['nextid']);
	}else if($neighbours['previd']>0){ //if has previous
		$infos = getNextInfo($neighbours['previd']);
		$infos['is_prev'] = true;
	}

	if(!empty($infos)){
		$infos['is_single'] = true;
		$isPrev = isset($infos['is_prev']) && $infos['is_prev'];
		$infos['slogan'] = ($isPrev) ? $label." Précédent" : $label." Suivant" ;
		return $infos;
	}
}

function get_next_page_info(){
	$type = get_post_type();
	if($type == "page"){
		$page = get_post()->post_name;
		if($page == "art-lease"){
			return getMainPageInfo(get_page_by_path('digit-artlease'));
		}
		if($page == "digit-artlease"){
			return getMainPageInfo(get_page_by_path('marketing-suite'));
		}
		if($page == "marketing-suite"){
			return getMainPageInfo(get_page_by_path('work-lease'));
		}
		if($page == "work-lease"){
			return getMainPageInfo(get_page_by_path('eco-bulle'));
		}
		if($page == "eco-bulle"){
			return getMainPageInfo(get_page_by_path('art-lease'));
		}
	}
	elseif($type == 'artiste'){
		$current_type = get_post_type(); 
		// var_dump($current_type);
		$args = array( 
		    'post_type' => $current_type,
		    'orderby'  => 'post_date',
		    'order'    => 'DESC'
		);

		return getNextPageInfo('Artiste', $args);
	}
	elseif(is_single()){
		$cat = get_the_category(); 
		$current_cat_id = $cat[0]->cat_ID;

		$args = array( 
		    'category' => $current_cat_id,
		    'orderby'  => 'post_date',
		    'order'    => 'DESC',
		    'numberposts' => -1,
		);

		return getNextPageInfo('Article', $args);
	}
}


function getNeighbourIDsFromPosts($args){
	$post_id = get_the_ID(); // current post ID
	

	$posts = get_posts( $args );
	// get IDs of posts retrieved from get_posts
	$ids = array();
	foreach ( $posts as $thepost ) {
	    $ids[] = $thepost->ID;
		// var_dump(get_the_title($thepost->ID));
	}
	// get and echo previous and next post in the same category
	$thisindex = array_search( $post_id, $ids );

	$neighbours = array();
	$neighbours['previd']= isset( $ids[ $thisindex - 1 ] ) ? $ids[ $thisindex - 1 ] : -1;
	$neighbours['nextid']= isset( $ids[ $thisindex + 1 ] ) ? $ids[ $thisindex + 1 ] : -1;
	// if ($neighbours['previd'] == $neighbours['nextid']){
	// 	$neighbours['nextid'] = null;
	// }


	return $neighbours;
}


function get_gallery_title(){
	$type = get_post_type();
	if($type == "page"){
		$page = get_post()->post_name;
		if($page == "digit-artlease"){
			return "Les oeuvres";
		}
		return "Nos réalisation";
	}
}

// if ( function_exists( 'add_theme_support' ) ) { 
//     add_theme_support( 'post-thumbnails' );
    
   
// }







