
<article class="single">
	<h1 class="single-title"><?php the_title(); ?></h1>

	<div class="post-info">
		<div class="date">
			<?php the_time( 'j F Y' ); ?>
		</div>
		<?php
			$posttags = get_the_tags();
			if ($posttags) : ?>
				<h3 class="categorie">
					<?php echo $posttags[0]->name; ?>
				</h3>
		<?php endif; ?>
	</div>
	<div class="post-content row around-md">
		<div class="next-post col-xs-12 col-md-4 ">
			<?php 
				$next_post = get_previous_post();
				$prev_post = get_next_post();
				$hasNext = $hasPrev = false;
				$nextLink = $prevLink = '#';
				$otherPost = null;

				if(is_a( $next_post , 'WP_Post' )){
					$label = 'Suivant';
					$cat = get_the_category($next_post->ID)[0];
					if($cat->category_nicename == 'actualites'){
						$otherPost = $next_post;
						$nextLink = get_permalink( $otherPost->ID );
						$hasNext = true;
					}
				} 
				
				if(is_a( $prev_post , 'WP_Post' )){
					$cat = get_the_category($prev_post->ID)[0];
					if($cat->category_nicename == 'actualites'){
						$prevLink = get_permalink( $prev_post->ID );
						$hasPrev = true;
						if(!$hasNext){
							$label = 'Précédent';
							$otherPost = $prev_post;
						}
					}
				}
			?>
			<h4 class="next-post-label">Article <?php echo $label ?></h4>
			<h4 class="next-post-title"><a  href="<?php echo get_permalink( $otherPost->ID ); ?>"><?php echo get_the_title( $otherPost->ID ); ?></a></h4>
			
			<div class="next-post-arrows row center-xs">
				<a href="<?php echo $prevLink ?>" class="<?php if($hasPrev) echo 'enable';?> left">
					<?php 
						$path = __DIR__ .'/img/surrounded-arrow.svg';
						echo file_get_contents($path) ;
					?>
				</a> 
				<a href="<?php echo $nextLink ?>" class="<?php if($hasNext)  echo 'enable';?> right">
					<?php 
						$path = __DIR__ .'/img/surrounded-arrow.svg';
						echo file_get_contents($path) ;
					?>
				</a>
			</div>					
			
		</div>
		<?php $img = get_field('cover'); ?>
		<div class="single-content col-xs-12 col-md-7">
			<?php if (!empty($img)) : ?>	
				<div class="single-cover">
					<?php echo wp_get_attachment_image($img, 'large') ?>
				</div>
			<?php endif; ?>
	
			<div class="post-inner-content">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</article>
