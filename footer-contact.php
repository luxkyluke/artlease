<?php  
	//recuperation des infos contact
	$page = get_page_by_title('Contact');
	if (isset($page) ){
		$bg_color = get_field_object('bg_color', $page->ID)['value'];
		$style = '"background-color : '.$bg_color.'"';		
	}else{
		echo 'contact info not found';
	}
?>
		</div><!-- close #page -->
		<footer id="footer" >
			<div id="footer-contact" class="footer-contact" style=<?php  echo $style?>>
				<div class="footer-wrapper">
					<?php include(__DIR__ .'/contact.php') ;?>
					<div class="top-page">
						<a href="#top">
							<div class="top-page-wrapper">
								<p class="label">haut de page</p>
								<img src="<?php echo get_template_directory_uri(); ?>/img/top-arrow.svg" alt="flèche-haut" class="arrow">
							</div>
						</a>
					</div>
				</div>
				<div class="copyright">
					<strong>&copy <?php echo getdate()['year']?> ART LEASE </strong> - site by <a href="http://www.antoinedemiere.com" target="_blank" >Antoine DEMIERE</a>	
				</div>
			</div>
			<?php 
				$nextPage = get_next_page_info();
				if(isset($nextPage)):
					$isPrev = isset($nextPage['is_prev']) && $nextPage['is_prev'];
					$isSingle = isset($nextPage['is_single']) && $nextPage['is_single'];
			?>
			<div
				id="footer-next-page" 
				class="footer-next-page" 
				data-background-img="<?php echo $nextPage['bg']; ?>"
				data-img-tablet="<?php echo $nextPage['bg-mobile']; ?>"
				>
				<a href="<?php echo $nextPage['url'] ?>" class="next-page-link"></a>
				<div class="background-layer"></div>
				<div class="inner-content">
					<div class="<?php echo ($isPrev) ? 'text-wrapper prev' : 'text-wrapper';?>">
						<?php if($isSingle) :?>
							<div class="next-page-title-wrapper single">
								<div class="slogan single">
									<?php echo $nextPage['slogan'] ?>
								</div>
							</div>
							<h2 class="next-page-title single">
								<?php echo getFatTitle($nextPage['title']);?>
							</h2>
						<?php else: ?>
							<div class="next-page-title-wrapper">
								<h2 class="next-page-title">
									<?php echo getFatTitle($nextPage['title']);?>
								</h2>
							</div>
							<div class="slogan">
								<?php echo $nextPage['slogan'] ?>
							</div>
						<?php endif; ?>
					</div>
					<div class="<?php echo ($isPrev) ? 'button prev' : 'button';?>">
						<?php 
							$path = __DIR__ .'/img/surrounded-arrow.svg';
							echo file_get_contents($path) ;
						?>
					</div>
				</div>
			</div>
		<?php endif; ?>
		
		</footer>
		<?php wp_footer(); ?>
	</div><!-- close #scroll-wrapper -->
	
	
</body>
</html>
