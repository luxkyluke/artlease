<div class="post col-xs-12 " data-aos="fade-up">
	<!-- inner-content -->
	<div class="inner-content">
		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<?php echo get_the_excerpt(); ?>
			<?php
				$date = get_the_date();
			?>

		<div class="post-footer">
			<div class="button">
				<a href="<?php the_permalink(); ?>" class="button">En savoir <span>plus</span></a>
			</div>
			<div class="date">
				<?php
					echo $date;
				?>
			</div>
		</div>
	</div>
	<div class="post-thumbnail">
		<a class="post-thumbnail-link" href="<?php the_permalink(); ?>">  
			<img class="post-thumbnail-img" src="<?php echo wp_get_attachment_image_src(get_field('cover'), 'extract-thumb')[0]; ?>" alt="">
		</a>
	</div>
	<!-- /inner-content -->
</div>
