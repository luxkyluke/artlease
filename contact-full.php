<div id="contact-full" class="contact-full">
	<div class="frame"></div>
	<div class="content-wrapper">
		<?php include(__DIR__ .'/contact.php') ;?>
	</div>
	<a href="" class="contact-full-cross" id="contact-full-close">
		<img src="<?php echo get_template_directory_uri(); ?>/img/cross.svg" alt="close">
	</a>
</div>