<?php 
	get_header('home'); 


	$page = get_page_by_title('Home');
	$pageId = $page->ID;

	$homeImg = get_field('home_image', $pageId);
	$sections = Array();
?>

<div id="home-page">
	<section class="section main" >
		<div 
			class="background-layer" 
			data-background-img="<?php echo $homeImg['url'] ;?>"
			data-img-tablet="<?php echo $homeImg['sizes']['large'] ;?>"
		></div>
		<div class="overlay"></div>
		<div class="content">
			<?php echo get_field('main_slogan', $pageId)?>
		</div>
	</section>
	
	<?php if( have_rows('sections', $pageId) ): $i=0?>

		<section class="sections" >

		<?php while( have_rows('sections', $pageId) ): the_row(); 

			// vars
			$image = get_sub_field('background_image', $pageId);
			$slogan = get_sub_field('slogan', $pageId);
			$title = get_sub_field('title', $pageId);
			$link = get_sub_field('page', $pageId);

			$section = Array();
			$section["slogan"] = $slogan;
			$section["title"] = $title;
			$section["link"] = $link;

			array_push($sections, $section);

			?>
			<a href="<?php echo $link; ?>">
				<section id="<?php echo 'section-'.$i++; ?>" class="section">
					<div  
						class="background-layer" 
						data-background-img="<?php echo $image['url'] ;?>"
						data-img-tablet="<?php echo $image['sizes']['large'] ;?>"
					></div>
					<div class="overlay"></div>
				</section>
			</a>


		<?php endwhile; ?>

		<div class="content">
			<div class="content-wrapper">
				<a href="#" id="content-link">
					<div class="title-wrapper">
						<?php foreach ($sections as $i => $s) :
								$current = ($i == 0) ? "current" : "";
								$title = $s['title'];


							?>
							<div id="<?php echo 'title-'.$i;?>" class="<?php echo 'content-title '.$current; ?>"><?php echo getFatTitle($title); ?></div>
						<?php endforeach; ?>
						<!-- <div class="overlay"></div> -->
					</div>
					<div class="slogan" id="content-slogan">
						slogan
					</div>

					<div class="more-btn">
						<div href="#" id="more-link" class="button">
							en savoir <strong>plus</strong>
						</div>
					</div>
				</a>
			</div>
		</div>
		<div class="navigator">
			<ul class="navigator-list">
				<?php foreach ($sections as $i => $s) :
						$current = ($i == 0) ? "current" : ""
					?>
					<a href="#" data-id="<?php echo $i; ?>" class="navigator-link">
						<li class="<?php echo 'navigator-item '.$current; ?>">
							<span class="number">
								0<?php echo($i+1); ?>
							</span>
							<span class="dot"></span>
						</li>
					</a>
				<?php endforeach; ?>
			</ul>
			<a href="#" class="next-arrow">
				<?php echo file_get_contents(__DIR__."/img/top-arrow.svg");?>
			</a>
		</div>
		</section>

	<?php 
		endif; 

		echo '<script>';
		echo 'var homeSections = ' . json_encode($sections) . ';';
		echo '</script>';
	
	?>
	<a href="#" id="next-arrow" class="next-arrow">
		<?php echo file_get_contents(__DIR__."/img/large-arrow.svg");
		?>
	</a>
</div>
<?php get_footer(); ?>