<?php 
 	$items = get_gallery_items();
	// $size = 'medium'; // (thumbnail, medium, large, full or custom size)

 	echo '<script>';
	echo 'var galleryItems = ' . json_encode($items) . ';';
	echo '</script>';

	if(sizeof($items) < 1 && ! is_single()){
		echo '<p class="gallery-empty">Aucunes réalisations pour le moment...</p>';
	}

	$imageSeo = null;
	$url = get_the_permalink();

	/* détection du paramètre get (?image=XX) */
	if(!empty($_GET) && $_GET['image']){
		foreach($items as $it){
			if(isset($it['file']) && $it['file'] == $_GET['image']){
				$imageSeo = $it;
				// var_dump($imageSeo);
			}
		}
	}
?>

<div id="gallery" class="gallery">
	<div class="uk-margin">
        <div id="parralax-grid" class="uk-grid" data-uk-grid-parallax="">
			<?php if( $items ):
				// $i=0;
	        	foreach( $items as $item ):
					$domainColor = '#8b9c95';
	        		if(class_exists('Imagick')){
		        		$img = new Imagick($item['small-img']);
						$img->resizeImage(250, 250, Imagick::FILTER_GAUSSIAN, 1);
						$img->quantizeImage(1, Imagick::COLORSPACE_RGB, 0, false, false);
						$img->setFormat('RGB');

						$domainColor = '#'.substr(bin2hex($img), 0, 6);
					}
					$isVideo = isset($item['isVideo']) && $item['isVideo'];
	        		$link = ""; 	
	        		if(isset($item['file'])){ // si c'est une image on ajoute le lien pour le SEO
	        			$link .= $url."?image=".$item['file'];
	        		}
	        		$link .= "#oeuvre?id=".$item['id']; 	
	        		?>
	        		<div class="gallery-photo">
			            <a
			            	class="gallery-photo-link" 
			            	href="<?php echo $link;?>"
			            	data-img-id="<?php echo $item['id'];?>"
			            	>
			            	<div class="uk-grid-margin" data-background-img="<?php echo $item['large-img'];?>" data-is-gallery="true">
				            	<?php if ($isVideo) : ?>
					            	<div class="video-play">
					            		<?php 
											$path = __DIR__ .'/img/play.svg';
											echo file_get_contents($path) ;
										?>
					            	</div>
				            	<?php endif; ?>
			            		<div class="<?php echo (!empty($item['caption'])) ? 'gallery-photo-overlay hover' : 'gallery-photo-overlay' ;?>" style="<?php echo 'background-color: '.$domainColor; ?>">
			            			<div class="gallery-photo-info">
			            				<h3 class="caption"><?php echo $item['caption'];?></h3>
			            				<h3 class="description"><?php echo $item['description'];?></h3>
			            			</div>
			            		</div>
			            	</div>
		            	</a>
		        	</div>
		        <?php endforeach; 
			endif;?>
        </div>
    </div>
</div>

<div id="gallery-full" >
	<div class="frame"></div>
	<div class="gallery-full-center">
		<div class="gallery-full-content-container">
			<div class="loader">
				<?php 
					$path = __DIR__ .'/img/loader.svg';
					echo file_get_contents($path) ;
				?>
			</div>
			<div class="gallery-full-content">
				<?php if($imageSeo) :?>
					<img src="<?php echo $imageSeo['large-img'] ?>" alt="<?php echo $imageSeo['alt'] ?>" id="gallery-full-image" class="content ">
				<?php else : ?>	
					<img src="#" alt="" id="gallery-full-image" class="content ">
				<?php endif; ?>
				<iframe 
					id="gallery-full-video" 
					class="video content " 
					src=""  
					frameborder="0" 
					webkitAllowFullScreen 
					mozallowfullscreen 
					allowFullScreen
					></iframe>
				

				<div class="gallery-full-details" >
					<div class="background-layer"></div>
					<div id="gallery-full-info-overlay" class="overlay hide"></div>
					<div class="details-content">
						
					</div>
				</div>

			</div>
		</div>
		<div class="gallery-full-infos-bottom hide">
			<div class="gallery-full-infos-wrapper">
				<div class="gallery-full-infos-container">
					<h2 class="gallery-full-title text"></h2>
					<p class="gallery-full-artist text"></p>
					<div class="gallery-full-details-btn"><a href="#" id="gallery-full-info-btn">
						<?php 
							$path = __DIR__ .'/img/info.svg';
							echo file_get_contents($path) ;
						?>
					</a></div>
				</div>
			</div>
		</div>
	</div>
	<!-- <div class="gallery-full-arrows"> -->
	<div class="left-arrow arrow">
		<div class="arrow-wrapper">
			<?php echo file_get_contents(__DIR__ .'/img/large-arrow.svg') ;?>
			<a href="#" data-dir="-1">	</a>
		</div>
	</div>
	<div class="right-arrow arrow">
		<div class="arrow-wrapper">
			<?php echo file_get_contents(__DIR__ .'/img/large-arrow.svg') ;?>
			<a href="#" data-dir="1"></a>
		</div>
	</div>
	<!-- </div> -->
	
	<a href="" class="gallery-full-cross" id="gallery-full-close">
		<img src="<?php echo get_template_directory_uri(); ?>/img/cross.svg" alt="close">
	</a>
	<div class="details-content full"></div>

</div>
