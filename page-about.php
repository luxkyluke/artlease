<?php get_header(); ?>


<!-- site-content -->
<div class="site-content">
	<?php
	if ( have_posts() ) :
		while ( have_posts() ) :
			the_post();
			get_template_part( 'content', 'about' );
		endwhile;
		else :
			get_template_part( 'content', 'none' );
		endif;
		?>
</div>
<!-- /site-content -->
<!-- container -->
<?php get_footer('contact'); ?>
