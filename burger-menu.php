<div id="burger-menu">
	<div class="frame"></div>

	<a href="<?php echo home_url(); ?>" class="logo" >
		<img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="Logo">
	</a>
	<nav class="navigation">
		<?php echo getBurgerMenu(); ?>
	</nav>
	<div class="copyright">
		<strong>&copy <?php echo getdate()['year']?> ART LEASE </strong> - site by <a href="http://www.antoinedemiere.com" target="_blank" >Antoine DEMIERE</a>	
	</div>
</div>