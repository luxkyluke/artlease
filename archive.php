<?php get_header(); ?>
<!-- container -->
<div id="container" class="container documentation" data-hide-header-scroll="true">	
	<!-- site-content -->
	<div class="site-content">
		<?php if ( have_posts() ) : ?>
		<h1 class="page-title title">
		<?php single_cat_title();	?>
		</h1>

		<!-- main-column -->
		<div id="actuality" class="main-column">
			<?php
			$cpt = 0;


			while ( have_posts() ) :
				the_post();
				$cpt++;
				$odd = ($cpt%2) ? '' : 'odd'; 
				echo '<div class="post-wrapper row '.$odd.'">'
				?>
					<?php
						get_template_part( 'content-extract', get_post_format() );
					?>
				</div>
				<?php
			endwhile;
			?>
		</div>
	
		<!-- /main-column -->
		<?php
			global $wp_query; // you can remove this line if everything works for you

			// don't display the button if there are not enough posts
			if (  $wp_query->max_num_pages > 1 ):
				echo '<script>';
				echo 'var ajaxUrl = "' . site_url() . '/wp-admin/admin-ajax.php";';
				echo 'var posts = ' . json_encode( $wp_query->query_vars )  . ';';
				$cp = get_query_var( 'paged' ) ? get_query_var('paged') : 1 ;
				echo 'var currentPage = ' . $cp . ';';
				echo 'var maxPage = ' . $wp_query->max_num_pages  . ';';
				echo '</script>';
				?>
				<div class="more-content">
					<a 	href="#" id="more-btn">
						<img src="<?php echo get_template_directory_uri(); ?>/img/plus.svg" alt="plus">
					</a>
				</div>
		<?php endif; ?>

		<?php
		else :
			get_template_part( 'content', 'none' );
		endif;
		?>

		


	</div>
	<!-- /site-content -->

	<?php //get_sidebar(); ?>
</div>
<!-- /container -->
<?php get_footer('contact'); ?>
