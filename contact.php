<?php  
	//recuperation des infos contact
	$page = get_page_by_title('Contact');
	if (isset($page) ){
		$bureau = get_field_object('bureau', $page->ID)['value'];
		$siege = get_field_object('siege', $page->ID)['value'];
		$telephone = get_field_object('telephone', $page->ID)['value'];
		$email = get_field_object('email', $page->ID)['value'];
		$bg_color = get_field_object('bg_color', $page->ID)['value'];
		$style = '"background-color : '.$bg_color.'"';

		$siegeMapsLink = getGoogleMapsLink(strip_tags($siege));
		$bureauMapsLink = getGoogleMapsLink(strip_tags($bureau));

		$mailStr = strip_tags($email);
		$mailTo = 'mailto:'.strip_tags($email);
		$explodeEmail = explode('@', $mailStr);

		$telRaw = strip_tags($telephone);
		$explodeTel = explode(' ', $telRaw, 2);

	}else{
		echo 'contact info not found';
	}
?>

<div id="contact" class="contact">
	<div class="inner-content">
		<a href="<?php echo home_url(); ?>">
			<div class="logo">
				<img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="Logo" >
			</div>
		</a> 
		<div class="contact-info">
			<h3 class="title">Contact</h3>
			<div class="info-wrapper">
				<div class="siege-info">
					<h4 class="info-label">Siège</h4>
					<div class="info-coord">
						<a href="<?php echo $siegeMapsLink ?>" target="_blank">
							<div class="info-coord-item">
								<img src="<?php echo get_template_directory_uri(); ?>/img/location.svg" alt="position" class="info-coord-icon">
								<?php echo $siege ?>
							</div>
						</a>
						<a href="<?php echo $mailTo?>">
							<div class="info-coord-item">
								<img src="<?php echo get_template_directory_uri(); ?>/img/mail.svg" alt="email" class="info-coord-icon">
								<?php echo $email ?>
							</div>
						</a>
						<a href="tel:<?php echo $telRaw ?>">
							<div class="info-coord-item">
								<img src="<?php echo get_template_directory_uri(); ?>/img/phone.svg" alt="téléphone" class="info-coord-icon">
								<?php echo $telephone ?>
							</div>
						</a>

					</div>
				</div>
				<div class="bureau-info">
					<h4 class="info-label">Bureaux</h4>
					<div class="info-coord">
						<a href="<?php echo $bureauMapsLink ?>" target="_blank">
							<div class="info-coord-item">
								<img src="<?php echo get_template_directory_uri(); ?>/img/location.svg" alt="position" class="info-coord-icon">
								<?php echo $bureau ?>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>